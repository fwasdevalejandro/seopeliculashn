<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User; 
use App\Post;
use App\Categorias;
use Auth;
use File;
use Validator;


class Categoriass extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    
    public function show($slug)
    {
        //Todas las Categorias... eso espero
        $Categorias = Categorias::where('slug', '=', $slug)->firstOrFail();
        //Todas las Peliculas... espero un poco bug
        $Peliculass = Post::where('Categorias_id','=', $Categorias->id)->latest()->paginate(10);
        //Todas las Categorias
        $Categores = Categorias::simplePaginate(10);
        return view('Pages.Cat.show',compact('Peliculass','Categorias','Categores'));
       
    }

    public function ShowPelicula($slug)
    {
        $Peliculas = Post::all();
        //Todas las Categorias... eso espero
        $Peliculass = Post::where('slug', '=', $slug)->firstOrFail();
        //Todas las Peliculas... espero un poco bug
        return view('Pages.Posts.post',compact('Peliculass','Peliculas'));
       
    }
}
