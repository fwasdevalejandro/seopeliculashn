<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\ImageUpload;
use App\User; 
use App\Categorias;
use App\Gallery;
use App\Post;
use Auth;
use File;
use Validator; 

class HomeController extends Controller
{
      /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    	$Categories =  Categorias::all();
        $Peliculass =  Post::latest()->paginate(12);
        $BigPeliculass =  Post::oldest()->paginate(6);
        $TVSeries = Post::where('Categorias_id','=', '34')->latest()->paginate(10);
        return view('Pages/home',compact('Categories','Peliculass','BigPeliculass','TVSeries'));
    }

}
