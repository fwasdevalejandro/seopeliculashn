<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\Categorias;
use App\Instagram;
use Auth;

class Peliculass extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $PeliculasDes =  Post::latest()->paginate(12);
        return view('Pages.Posts.post',compact('PeliculasDes'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        // This query to get Peliculass page //
        $Peliculas = Post::where('slug', '=', $slug)->firstOrFail();
        //To Get All Peliculass recents OUT SIDE IN HOME VIEW
        $TrendPeliculass = Post::paginate(5)->fresh(); 
        // To Get Change The Language
        return view('Pages.Peliculass.show',compact('Peliculas','TrendPeliculass'));
    }
}
