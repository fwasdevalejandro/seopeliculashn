@extends('Dashboard.main')

@section('Dashboard')
<!-- START PAGE CONTENT -->
<div class="content ">
  <!-- START PAGE COVER -->
  <div class="jumbotron page-cover" data-pages="parallax">
    <div class=" container-fluid  container-fixed-lg">
      <div class="inner">
        <!-- START BREADCRUMB -->
        <ol class="breadcrumb">  
          <li class="breadcrumb-item"><a href="{{ url('admin') }}">{{ __('Dashboard')}}</a></li>
          <li class="breadcrumb-item"><a href="{{ route('Posts.index') }}">{{ __('Peliculass')}}</a></li>
          <li class="breadcrumb-item active">{{ __('Create Peliculass')}}</li>
        </ol>
        <!-- END BREADCRUMB -->
        <div class="container-md-height">
          <div class="row">
            <!-- START card -->
            <div class="card card-transparent">
              <div class="card-body">
                <h3><i class="pg-Peliculas"></i>{{ __('add Peliculass')}} </h3>
              </div>
            </div>
            <!-- END card -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END PAGE COVER -->
  <!-- START CONTAINER FLUID -->
  <div class=" container-fluid container-fixed-lg">
    <!-- START card -->
    <div class="row">
      <div class="col-lg-9">
        <!-- START card -->
        <div class="card card-default">
          <div class="card-header ">
            <div class="card-title">
              {{ __('Create your Peliculass')}}
            </div>
          </div>
          @if ($errors->any())    
          <div class="pgn-wrapper top-inline" data-position="top">
            <div class="pgn push-on-sidebar-open pgn-bar">
              <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">{{ __('Close')}}</span></button>
                @foreach ($errors->all() as $error)
                {{ $error }}
                @endforeach
              </div>
            </div>
          </div>
          @endif
          <div class="card-body">
           <form action="{{ route('Posts.store') }}" method="POST"  role="form" enctype="multipart/form-data"> 
              @csrf
            <div class="form-group form-group-default required">
              <label for="Title_en">{{ __('Peliculass Title')}}</label>
              <input type="text" class="form-control @error('Title_en') is-invalid @enderror" required="" placeholder="ex:  The title for your Peliculass English" 
                     id="Title_en" name="Title_en">
              @error('Title_en')
                <div class="alert alert-danger">{{ $message }}</div>
              @enderror 
            </div>
            <!-- START card -->
            <div class="card card-default">
              <div class="card-header">
                <div class="card-title">{{ __('Peliculass Content English')}}
                </div>
              </div>
              <div class="card-body no-scroll card-toolbar">
                <div class="summernote-wrapper">
                  <textarea  id='summernote' name="body_en"></textarea>
                </div>
              </div>
            </div>
            <!-- END card -->
            <!-- START card -->
            <div class="card card-transparent">
              <div class="card-header ">
                <div class="card-title">{{ __('Languages')}}
                </div>
              </div>
              <div class="card-body no-padding">
                <div class="row">
                  <div class="col-xl-12">
                    <div class="card card-transparent flex-row">
                      <ul class="nav nav-tabs nav-tabs-simple nav-tabs-left bg-white" id="tab-3">
                        <li class="nav-item">
                          <a href="#"  data-toggle="tab" data-target="#French">{{ __('French')}}</a>
                        </li>
                        <li class="nav-item">
                          <a href="#" data-toggle="tab" data-target="#Arabic">{{ __('Arabic')}}</a>
                        </li>
                      </ul>
                      <div class="tab-content bg-white col-xl-11">
                        <div class="tab-pane" id="French">
                          <div class="form-group form-group-default required">
                            <label>{{ __('Peliculass Title French')}}</label>
                            <input type="text" name="Title_fr" class="form-control" placeholder="ex:  The title for your Peliculass"> 
                          </div>
                          <!-- START card -->
                          <div class="form-group">
                            <textarea class="form-control" name="body_fr" placeholder="Peliculass Content French" rows="10"></textarea>
                          </div>
                        </div>
                        <div class="tab-pane active" id="Arabic">
                          <div class="form-group form-group-default required">
                            <label>{{ __('Peliculass Title Arabic')}}</label>
                            <input type="text" class="form-control" name="Title_ar" required="" placeholder="ex:  The title for your Peliculass"> 
                          </div>
                          <!-- START card -->
                          <div class="form-group">
                            <textarea class="form-control" name="body_ar" placeholder="Peliculass Content Arabic" rows="10"></textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- END card -->
            <button class="btn btn-success btn-cons btn-animated from-left fa fa-save pull-right" type="submit">
              <span>{{ __('save')}}</span>
            </button>
          </div>
        </div>
        <!-- END card -->
      </div>
      <div class="col-lg-3">
        <!-- START card -->
        <div class="card card-default">
          <div class="card-header ">
            <div class="card-title">{{ __('SEO Content')}}</div>
          </div>
          <div class="card-body">
              <p>{{ __('These are Default Peliculass Category')}}</p>
              <div class="form-group form-group-default form-group-default-select2 required">
                <label class="">{{ __('Category')}}</label>
                <select class="full-width" data-placeholder="Select Category" data-init-plugin="select2" name="category_id">
                  <!-- ============================================= links Content Start Setting ============================================= -->
                  @if($Categores !== NULL)
                  @foreach($Categores as $Category)
                  <option value="{{ $Category->id }}">{{ $Category->Title_en }}</option>
                  @endforeach
                  @else
                  <option value="0">{{ __('NO Category')}}</option>
                  @endif
                  <option value="0">{{ __('NO Category')}}</option>
                  <!-- ============================================= links Content Start Setting ============================================= -->
                </select>
              </div>
              <p>{{ __('These are Default Peliculass Author')}} </p>
              <div class="form-group form-group-default form-group-default-select2 required">
                <label>{{ __('Author')}}</label>
                <select class="full-width" data-placeholder="Select Author" data-init-plugin="select2" name="author_id">
                  <!-- ============================================= links Content Start Setting ============================================= -->
                  @if($Users !== NULL)
                  @foreach($Users as $Author)
                  <option value="{{ $Author->id }}">{{ $Author->name }}</option>
                  @endforeach
                  @else
                  <option value="0">{{ __('NO Author')}}</option>
                  @endif
                  <option value="0">{{ __('NO Author')}}</option>
                  <!-- ============================================= links Content Start Setting ============================================= -->
                </select>
              </div>
            <div class="form-group form-group-default input-group">
              <div class="form-input-group">
                <label>{{ __('Meta Description')}}</label>
                <input type="text" class="form-control" placeholder="Peliculass Content Meta" name="meta_description">
              </div>
            </div>
            <div class="form-group form-group-default input-group">
              <div class="form-input-group">
                <label>{{ __('LINK Peliculass')}}</label>
                <input type="text" class="form-control" placeholder="LINK Peliculass" name="meta_keywords">
              </div>
            </div>
            <div class="form-group form-group-default input-group">
              <div class="form-input-group">
                <label>{{ __('Size  20G - 10MB')}}</label>
                <input type="text" class="form-control" placeholder="Peliculass Size" name="seo_title">
              </div>
            </div>
            <div class="form-group form-group-default input-group">
              <div class="form-input-group">
                <label>{{ __('Downloud')}}</label>
                <input type="text" class="form-control" placeholder="Downloud Link" name="Downloud">
              </div>
            </div>
            <label>{{ __('Featured')}}</label>
            <input type="checkbox" data-init-plugin="switchery" checked="checked" name="featured" />
            </form>
          </div>
        </div>
        <!-- END card -->
        <!-- START card -->
        <div class="card card-default">
          <div class="card-header ">
            <div class="card-title">
              {{ __('Drag and drop Peliculass image Here')}} 
            </div>
          </div>
          <div class="card-body no-scroll no-padding">
            <form method="post" action="{{url('Dashboard/image/upload/store')}}" enctype="multipart/form-data" class="dropzone" id="dropzone">
              @csrf
            </form> 
        </div>
      </div>
      <!-- END card -->
    </div>
  </div>
  <!-- END card -->
</div>
<!-- END CONTAINER FLUID -->
</div>
<!-- END PAGE CONTENT -->
@endsection