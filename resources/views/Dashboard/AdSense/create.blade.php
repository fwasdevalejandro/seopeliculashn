@extends('Dashboard.main')

@section('Dashboard')
<div class="content">
  <!-- START PAGE COVER -->
  <div class="jumbotron page-cover" data-pages="parallax">
    <div class=" container-fluid  container-fixed-lg">
      <div class="inner">
        <!-- START BREADCRUMB -->
        <ol class="breadcrumb">  
          <li class="breadcrumb-item"><a href="{{ url('admin') }}">{{ __('Dashboard')}}</a></li>
          <li class="breadcrumb-item"><a href="{{ route('AdSense.index') }}">{{ __('Ads')}}</a></li>
          <li class="breadcrumb-item active">{{ __('Create Ads')}}</li>
        </ol>
        <!-- END BREADCRUMB -->
        <div class="container-md-height">
          <div class="row">
            <!-- START card -->
            <div class="card card-transparent">
              <div class="card-body">
                <h3><i class="pg-grid"></i>{{ __('add Ads')}} </h3>
              </div>
            </div>
            <!-- END card -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END PAGE COVER -->
  <!-- START CONTAINER FLUID -->
  <div class=" container-fluid container-fixed-lg">
    <!-- START card -->
    <div class="row">
      <div class="col-lg-8">
        <!-- START card -->
        <div class="card card-default">
          <div class="card-header ">
            <div class="card-title">
              {{ __('Create your Ads')}}
            </div>
          </div>
          @if ($errors->any())    
          <div class="pgn-wrapper top-inline" data-position="top">
            <div class="pgn push-on-sidebar-open pgn-bar">
              <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">{{ __('Close')}}</span></button>
                @foreach ($errors->all() as $error)
                {{ $error }}
                @endforeach
              </div>
            </div>
          </div>
          @endif
          <div class="card-body">
            <form action="{{ route('AdSense.store') }}" method="POST"  role="form" enctype="multipart/form-data"> 
              @csrf
            <div class="row">
              <p>{{ __('These are Default Ads Location')}}</p>
              <div class="form-group form-group-default form-group-default-select2 required">
                <label for="Location">{{ __('Location')}}</label>
                <select class="full-width" data-placeholder="{{ __('Select Location')}}" data-init-plugin="select2" name="Location">
                  <option value="Home">{{ __('Home')}}</option>
                  <option value="Header">{{ __('Header')}}</option>
                  <option value="Footer">{{ __('Footer')}}</option>
                  <option value="Single">{{ __('Post Single')}}</option>
                  <option value="Other">{{ __('Other')}}</option>
                </select>
              </div>
            </div>
            <div class="form-group form-group-default required">
              <label for="name">{{ __('Ads Title')}}</label>
              <input type="text" class="form-control @error('name') is-invalid @enderror" required="" placeholder="{{ __('ex: This Title Ads')}}" name="name">
              @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
              @enderror  
            </div>
            <div class="form-group form-group-default required">
              <label for="Display">{{ __('Your Display Name')}}</label>
              <input type="text" class="form-control @error('name') is-invalid @enderror" required="" placeholder="{{ __('ex:  Your Display Name')}}" name="Display">
              @error('Display')
                <div class="alert alert-danger">{{ $message }}</div>
              @enderror 
            </div>
            <div class="row">
              <p>{{ __('These are Default Ads Type')}}</p>
              <div class="form-group form-group-default form-group-default-select2 required">
                <label for="Type">{{ __('Type')}}</label>
                <select class="full-width" data-placeholder="{{ __('Select Type')}}" data-init-plugin="select2" name="Type">
                  <option value="Code">{{ __('Code')}}</option>
                  <option value="image">{{ __('image')}}</option>
                </select>
              </div>
            </div>
            <!-- START card -->
            <div class="card card-transparent">
              <div class="card-body no-padding">
                <div class="row">
                  <div class="col-xl-12">
                    <div class="card card-transparent flex-row">
                      <ul class="nav nav-tabs nav-tabs-simple nav-tabs-left bg-white" id="tab-3">
                        <li class="nav-item">
                          <a class="padding-left" href="#"  data-toggle="tab" data-target="#Image"><i class="pg-image"></i> {{ __('Image')}}</a>
                        </li>
                        <li class="nav-item">
                          <a href="#" data-toggle="tab" data-target="#AdSense" class="padding-left"><i class="fa fa-google"></i>{{ __('AdSense')}}</a>
                        </li>
                      </ul>
                      <div class="tab-content bg-white col-xl-11">
                        <div class="tab-pane" id="Image">
                          <div class="form-group form-group-default required">
                            <label for="url">{{ __('Your Url Image')}}</label>
                            <input type="text" class="form-control @error('url') is-invalid @enderror" placeholder="{{ __('ex:  Your Url Image')}}" name="url"> 
                          </div>
                        </div>
                        <div class="tab-pane active" id="AdSense">
                          <div class="card-header ">
                            <div class="card-title">{{ __('Google AdSense Code')}} 
                            </div>
                          </div>
                          <div class="card-body no-scroll card-toolbar">
                            <div class="summernote-wrapper">
                              <textarea  id='summernote' name="code"></textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- END card -->
            <button class="btn btn-success btn-cons btn-animated from-left fa fa-save pull-right mb-30" type="submit">
              <span>{{ __('save')}}</span>
            </button>
          </div>
        </div>
        <!-- END card -->
      </div>
      <div class="col-lg-4">
        <!-- START card -->
        <div class="card card-default">
          <div class="card-header ">
            <div class="card-title">{{ __('Control Ads')}}</div>
          </div>
          <div class="card-body">
            <label for="Active">{{ __('Active Ads')}}</label>
            <input type="checkbox" data-init-plugin="switchery" name="Active" />
          </div>
        </div>
        <!-- END card -->
        </form>
        <!-- START card -->
      <!-- END card -->
    </div>
  </div>
  <!-- END card -->
</div>
</div>
@endsection