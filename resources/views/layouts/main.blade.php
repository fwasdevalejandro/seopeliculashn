<!DOCTYPE html>
<html lang="zxx">
<head>
    <!-- ===================================== Meta site ================================================ -->
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <!-- ====== Laravel description site edit delete from admin panel ================== -->
    <meta name="description" content="{{ option('MetaDescription')  }}">
    <!-- ====== Laravel author site edit delete from admin panel ====================== -->
    <meta name="author" content="{{ option('Metaauthor')  }}">
    <!-- ====== Laravel keywords site edit delete from admin panel ================== -->
    <meta name="keywords" content="{{ option('MetaKeyWords')  }}">  
    <!-- ====== Laravel robots site edit delete from admin panel ================== -->
    <meta name="robots" content="{{ option('Metarobots') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- ====== Laravel favicon icon ================== -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/images/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/images/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/images/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/images/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/images/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/images/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/images/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/images/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/images/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('assets/images/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset(option('Favicon')) }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset(option('Favicon')) }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset(option('Favicon')) }}">
    <link rel="manifest" href="{{ asset('assets/images/favicon/manifest.json') }}">
    <!-- ====== Laravel favicon icon================== -->
    <title>{{ option('SiteTitle') }}</title>
    <!--- link stylesheet -->
    <link href="https://fonts.googleapis.com/css?family=Anton|Titillium+Web:300,400&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amiri:400,700&display=swap" rel="stylesheet">
    <!-- Bootstrap core CSS-->
    <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template-->
    <link href="{{ asset('assets/css/Corner.css') }}" rel="stylesheet">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/owl-carousel/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/owl-carousel/owl.theme.css') }}">
    <span class="display-zero">{{ $Locale = LaravelLocalization::getCurrentLocale() }}</span>
    @if($Locale == 'ar')
    <link rel="stylesheet" href="{{ asset('assets/css/rtl.css') }}" />
    @else
    @endif
</head>
<body id="page-top">
    <nav class="navbar navbar-expand navbar-light bg-white static-top Corner-nav sticky-top"> &nbsp;&nbsp;
        <a class="navbar-brand mr-1" href="{{ url('') }}">
            <img class="img-fluid" alt="{{ option('SiteTitle')  }}" src="{{ asset(option('logo')) }}">
        </a>
        <!-- ================================ Search form ================================ -->
        <script>
            function validateForm() {
              var x = document.forms["myForm"]["search"].value;
              if (x == null || x == "" || x.length > 30 ) {
               window.location.href = '{{ url('') }}';
               return false;
          }
        }
        </script>
   <form name="myForm" onsubmit="return validateForm()" class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-5 my-2 my-md-0 Corner-navbar-search" method="GET" action="{{ url('search') }}" role="search">
    @csrf
    <div class="input-group">
        <input class="form-control" type="text" placeholder="{{ __('main.Search')}}" name="search">
        <div class="input-group-append">
            <button class="btn btn-light" type="submit">
                <i class="fas fa-search"></i> 
            </button>
        </div>
    </div>
</form>

<ul class="navbar-nav ml-auto ml-md-0 Corner-right-navbar">
    <button class="btn btn-link btn-sm text-secondary order-1 order-sm-0" id="sidebarToggle">
        <i class="fas fa-bars"></i>
    </button>
    @guest
    <li class="nav-item dropdown no-arrow Corner-right-navbar-user">
        <a class="nav-link dropdown-toggle user-dropdown-link" href="#" id="userDropdown" role="button" 
        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <img alt="Avatar" src="{{ asset(option('logo')) }}">
    </a>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
        <a class="dropdown-item" href="{{ route('login') }}"><i class="fas fa-fw fa-user-circle"></i> &nbsp; {{ __('main.Login')}}</a>
        <a class="dropdown-item" href="{{ route('register') }}"><i class="fas fa-fw fa-cog"></i> &nbsp; {{ __('main.Register')}}</a>
        <div class="dropdown-divider"></div>
    </div>
</li>
@else
<li class="nav-item dropdown no-arrow Corner-right-navbar-user">
    <a class="nav-link dropdown-toggle user-dropdown-link" href="#" id="userDropdown" role="button" 
    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    @if(isset(Auth::user()->ImageUpload->filename))
    <img src="{{ asset(Auth::user()->ImageUpload->filename) }}" alt="{{ Auth::user()->name }}">
    @else
    @endif
</a>
<div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
    <a class="dropdown-item color-w"><i class="fas fa-fw fa-user-circle"></i> &nbsp; {{ Auth::user()->name }}</a>
    <div class="dropdown-divider"></div>
    <!-- ===============================  CONTENT ======================================== -->
    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
        <i class="fas fa-fw fa-sign-out-alt"></i>
    &nbsp; {{ __('main.Logout')}}</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="display-zero">
        @csrf
    </form>
    <!-- ===============================  CONTENT ======================================== -->
</div>
</li>
@endguest
</ul>
</nav>
<!-- /#wrapper -->
<!--********************************************************-->
<!--********************** SITE header *********************-->
<!--********************************************************-->
@section('sidebar')
<div id="wrapper">
    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
        @foreach(Menus() as $Menu)
        <li class="nav-item">
           <a class="nav-link" href="{{ url($Menu->url) }}" target="{{ $Menu->target }}">
               <i class="fas fa-fw fa-play-circle"></i>
               <span>{{ $Menu->{'Title_'.$Locale} }}</span>
           </a>
       </li>
       @endforeach
       @guest
       <li class="nav-item">
           <a class="nav-link" href="{{ route('login') }}">
               <i class="fas fa-fw fa-headset"></i>
               <span>{{ __('main.Login')}}</span>
           </a>
       </li>
       <li class="nav-item">
           <a class="nav-link" href="{{ route('register') }}">
               <i class="fas fa-fw fa-chess-queen"></i>
               <span>{{ __('main.Register')}}</span>
           </a>
       </li>
       @else
       @endguest
       <li class="nav-item dropdown">
           <a class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               <i class="fas fa-fw fa-list-alt"></i>
               <span>{{ __('main.Browse_categories')}}</span>
           </a>
           <div class="dropdown-menu">
              <!-- =============================== BASE ======================================== -->
              @foreach(Categories() as $Category)
              <a class="dropdown-item" href="{{ url('Cat') }}/{{ $Category->slug }}">{{ $Category->{'Title_'.$Locale} }}</a>
              @endforeach
          </div>
      </li>
      <li class="nav-item dropdown">
       <a class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           <i class="fas fa-fw fa-language"></i>
           <span>{{ __('main.languages')}}</span>
       </a>
       <div class="dropdown-menu">
          <!-- =============================== BASE ======================================== -->
          @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
          <a  class="dropdown-item" rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
            {{ $properties['native'] }}
        </a>
        @endforeach
    </div>
</li>
</ul>
@show
@yield('content')
<!--********************************************************-->
<!--********************** SITE FOOTER *********************-->
<!--********************************************************--> 
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
<!-- Bootstrap core JavaScript-->
<script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- Core plugin JavaScript-->
<script src="{{ asset('assets/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
<!-- Owl Carousel -->
<script src="{{ asset('assets/vendor/owl-carousel/owl.carousel.js') }}"></script>
<!-- Custom scripts for all pages-->
<script src="{{ asset('assets/js/custom.js') }}"></script>
</body>
</html>