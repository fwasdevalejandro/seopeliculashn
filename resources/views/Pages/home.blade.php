@extends('layouts.main')

@section('content')
<!--********************* SITE CONTENT *********************-->
<!--********************************************************-->
<span class="display-zero">{{ $Locale = LaravelLocalization::getCurrentLocale() }}</span>
<div id="content-wrapper">
  <div class="single-channel-page">
    <div class="single-channel-image">
     <img class="img-fluid" src="{{ asset(option('coverSettings')) }}">
     <div class="channel-profile w-50 color-fff">
      <h4 class="color-fff font-40">{{ __('main.Corner_Download')}}</h4>
      <p class="font-20">
       @if($Locale == 'ar')
       {{ option('Home_ar') }}
       @elseif($Locale == 'fr')
       {{ option('Home_fr') }}
       @else
       {{ option('Home_en') }}
       @endif 
     </p>
   </div>
 </div>
</div>
<div class="container-fluid pb-0">
 <div class="top-mobile-search">
  <div class="row">
   <div class="col-md-12">   
    <form class="mobile-search" method="GET" action="{{ url('search') }}" enctype="multipart/form-data" role="search">
     {{ csrf_field() }}
     <div class="input-group">
       <input type="search" placeholder="{{ __('main.Search')}}" name="search" class="form-control">
       <div class="input-group-append">
         <button type="submit" class="btn btn-dark"><i class="fas fa-search"></i></button>
       </div>
     </div>
   </form>    
 </div>
</div>
</div>
<div class="video-block section-padding">
  <div class="row">
   <div class="col-md-12">
    <div class="main-title">
     <div class="btn-group float-right right-action">
      <a class="btn btn-secondary btn-sm border-none" href="{{ url('Peliculas') }}">{{ __('main.Trend_Peliculass')}}</a>
    </div>
    <h6>{{ __('main.Trend_Peliculass')}}</h6>
  </div>
</div>
@foreach($Peliculass as $Peliculas)
<div class="col-xl-2 col-sm-4 mb-3">
  <div class="video-card">
   <div class="video-card-image">
    <a class="play-icon" href="{{ url('Peliculas') }}/{{$Peliculas->slug}}"></a>
    <a href="{{ url('Peliculas') }}/{{$Peliculas->slug}}">
     <img src="{{ asset($Peliculas->ImageUpload->filename) }}" alt="{{ $Peliculas->meta_description }}" class="img-fluid">
   </a>
   @if(isset($Peliculas->Category->{'Title_'.$Locale}))
   <div class="time">{{ $Peliculas->Category->{'Title_'.$Locale} }} </div>
   @else
   @endif
 </div>
 <div class="video-card-body">
  <div class="video-title">
   <a href="{{ url('Peliculas') }}/{{$Peliculas->slug}}">{{ $Peliculas->{'Title_'.$Locale} }}</a>
 </div>
 <div class="video-page text-color">
   @if(isset($Peliculas->Category->{'Title_'.$Locale}))
   {{ $Peliculas->Category->{'Title_'.$Locale} }}
   @else
   @endif
 </div>
 <div class="video-view">
  {{ __('main.Size')}} : {{ $Peliculas->seo_title }} <i class="fas fa-calendar-alt"></i> {{ date('M j, Y', strtotime($Peliculas->created_at)) }}
</div>
</div>
</div>
</div>
@endforeach
</div>
<div class="single-channel-image section-padding mb-4">
 <!-- ================ /New Adones content =================== -->
 @foreach(homeones() as $Ad)
 @if($Ad->Type == 'image')
 <a>
  <img class="img-fluid" src="{{ asset($Ad->url) }}" alt="{{ $Ad->Display }}" width="100%">
</a>
@elseif($Ad->Type == 'Code') 
{{ $Ad->code }}
@endif
@endforeach
<!-- ================ /New Adones content =================== -->
</div>
</div>
<hr class="mt-0">
<div class="video-block section-padding">
  <div class="row">
   <div class="col-md-12">
    <div class="main-title">
     <div class="btn-group float-right right-action">
      <a class="btn btn-secondary btn-sm border-none" href="{{ url('Peliculas') }}"> {{ __('main.Featured_Peliculass')}}</a>
    </div>
    <h6> {{ __('main.Featured_Peliculass')}}</h6>
  </div>
</div>
@foreach($BigPeliculass as $Peliculas)
<div class="col-xl-2 col-sm-4 mb-3">
  <div class="video-card">
   <div class="video-card-image">
    <a class="play-icon" href="{{ url('Peliculas') }}/{{$Peliculas->slug}}"></a>
    <a href="{{ url('Peliculas') }}/{{$Peliculas->slug}}">
     <img src="{{ asset($Peliculas->ImageUpload->filename) }}" alt="{{ $Peliculas->meta_description }}" class="img-fluid">
   </a>
   @if(isset($Peliculas->Category->{'Title_'.$Locale}))
   <div class="time">{{ $Peliculas->Category->{'Title_'.$Locale} }} </div>
   @else
   @endif
 </div>
 <div class="video-card-body">
  <div class="video-title">
   <a href="{{ url('Peliculas') }}/{{$Peliculas->slug}}">{{ $Peliculas->{'Title_'.$Locale} }}</a>
 </div>
 <div class="video-page text-color">
   @if(isset($Peliculas->Category->{'Title_'.$Locale}))
   {{ $Peliculas->Category->{'Title_'.$Locale} }}
   @else
   @endif
 </div>
 <div class="video-view">
  {{ __('main.Size')}} : {{ $Peliculas->seo_title }} <i class="fas fa-calendar-alt"></i> {{ date('M j, Y', strtotime($Peliculas->created_at)) }}
</div>
</div>
</div>
</div>
@endforeach
</div>
</div>
<div class="single-channel-image section-padding mb-4">
 <!-- ================ /New Adones content =================== -->
 @foreach(AdTwos() as $Ad)
@if($Ad->Type == 'image')
 <a>
  <img class="img-fluid" src="{{ asset($Ad->url) }}" alt="{{ $Ad->Display }}" width="100%">
</a>
@elseif($Ad->Type == 'Code')
{{ $Ad->code }}
@endif
@endforeach
<!-- ================ /New Adones content =================== -->
</div>
<hr class="mt-0">
<div class="video-block section-padding">
  <div class="row">
   <div class="col-md-12">
    <div class="main-title">
     <div class="btn-group float-right right-action">
      <a class="btn btn-secondary btn-sm border-none" href="{{ url('Peliculas') }}">{{ __('main.Popular_Series')}}</a>
    </div>
    <h6>{{ __('main.Popular_Series')}}</h6>
  </div>
</div>
@foreach($TVSeries as $Peliculas)
<div class="col-xl-2 col-sm-4 mb-3">
  <div class="video-card">
   <div class="video-card-image">
    <a class="play-icon" href="{{ url('Peliculas') }}/{{$Peliculas->slug}}"></a>
    <a href="{{ url('Peliculas') }}/{{$Peliculas->slug}}">
     <img src="{{ asset($Peliculas->ImageUpload->filename) }}" alt="{{ $Peliculas->meta_description }}" class="img-fluid">
   </a>
   @if(isset($Peliculas->Category->{'Title_'.$Locale}))
   <div class="time">{{ $Peliculas->Category->{'Title_'.$Locale} }} </div>
   @else
   @endif
 </div>
 <div class="video-card-body">
  <div class="video-title">
   <a href="{{ url('Peliculas') }}/{{$Peliculas->slug}}">{{ $Peliculas->{'Title_'.$Locale} }}</a>
 </div>
 <div class="video-page text-color">
   @if(isset($Peliculas->Category->{'Title_'.$Locale}))
   {{ $Peliculas->Category->{'Title_'.$Locale} }}
   @else
   @endif
 </div>
 <div class="video-view">
  {{ __('main.Size')}} : {{ $Peliculas->seo_title }} <i class="fas fa-calendar-alt"></i> {{ date('M j, Y', strtotime($Peliculas->created_at)) }}
</div>
</div>
</div>
</div>
@endforeach
</div>
</div>
</div>
<!-- /.container-fluid -->
<!-- Sticky Footer -->
<footer class="sticky-footer">
 <div class="container">
  <div class="row no-gutters">
   <div class="col-lg-6 col-sm-6">
    <p class="mt-1 mb-0 text-dark">{{ __('main.All_reserved')}} <strong class="text-dark">{{ option('SiteTitle')  }}</strong>.<br></p>
  </div>
  <div class="col-lg-6 col-sm-6 text-right">
    <div class="app">
     <a><img alt="{{ option('SiteTitle')  }}" src="{{ asset('assets/images/gplay.png') }}"></a>
     <a><img alt="{{ option('SiteTitle')  }}" src="{{ asset('assets/images/aplay.png') }}"></a>
   </div>
 </div>
</div>
</div>
</footer>
</div> 
@endsection
