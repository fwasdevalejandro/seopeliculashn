@extends('layouts.main')

@section('content')
<!-- ============================================================= Content Start ============================================================= -->
<span class="display-zero">{{ $Locale = LaravelLocalization::getCurrentLocale() }}</span>
<div id="content-wrapper">
  <div class="container-fluid pb-0">
   <div class="video-block section-padding">
    <div class="row">
     <div class="col-md-8">
      <div class="single-video-left">
       <div class="single-video">
        <iframe width="100%" height="415" src="{{ $Peliculas->meta_keywords }}?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      </div>
      <div class="single-video-title box mb-3">
        <h2><a>{{ $Peliculas->{'Title_'.$Locale} }}</a></h2>
        <p class="mb-0 color-000"><i class="fas fa-calendar-alt"></i> {{ date('M j, Y', strtotime($Peliculas->created_at)) }}</p>
        <a href="{{ $Peliculas->Downloud }}" class="btn-lg btn btn-secondary btn-dow" data-uk-tooltip="title: link Downloud" target="blank"><i class="fas fa-download"></i> {{ __('main.Download')}}  {{ $Peliculas->seo_title }} {{ __('main.MB')}}</a>
      </div>
      <div class="single-video-info-content box mb-3">
        <h6>{{ __('main.About')}} :</h6>
        <p class="color-000 text-dark">{{ $Peliculas->{'body_'.$Locale} }} </p>
        <h6>{{ __('main.Category')}} :</h6>
        <p class="color-000">
          @if(isset($Peliculas->Category->{'Title_'.$Locale}))
          {{ $Peliculas->Category->{'Title_'.$Locale} }}
          @else
          @endif
        </p>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="single-video-right">
     <div class="row">
      <div class="col-md-12">
       <div class="adblock">
        <div class="img">
          <!-- ================ /New Adones content =================== -->
          @foreach(singles() as $Ad)
          @if($Ad->Type == 'image')
 <a>
  <img class="img-fluid" src="{{ asset($Ad->url) }}" alt="{{ $Ad->Display }}" width="100%">
</a>
@elseif($Ad->Type == 'Code')
          {{ $Ad->code }}
          @endif
          @endforeach
          <!-- ================ /New Adones content =================== -->
        </div>
      </div>
      <div class="main-title">
        <h6>{{ __('main.Trend_Peliculass')}}</h6>
      </div>
    </div>
    <div class="col-md-12">
      @foreach($TrendPeliculass as $TrendPeliculas)
      <div class="video-card video-card-list">
        <div class="video-card-image">
         <a class="play-icon" href="{{ url('Peliculas') }}/{{$TrendPeliculas->slug}}"></a>
         <a href="{{ url('Peliculas') }}/{{$TrendPeliculas->slug}}">
           <img src="{{ asset($TrendPeliculas->ImageUpload->filename) }}" alt="{{ $TrendPeliculas->meta_description }}" class="img-fluid">
         </a>
         @if(isset($TrendPeliculas->Category->{'Title_'.$Locale}))
         <div class="time">{{ $TrendPeliculas->Category->{'Title_'.$Locale} }} </div>
         @else
         @endif
       </div>
       <div class="video-card-body">
         <div class="video-title">
          <a href="{{ url('Peliculas') }}/{{$TrendPeliculas->slug}}">{{ $TrendPeliculas->{'Title_'.$Locale} }}</a>
        </div>
        <div class="video-page text-danger">
          @if(isset($TrendPeliculas->Category->{'Title_'.$Locale}))
          {{ $TrendPeliculas->Category->{'Title_'.$Locale} }}
          @else
          @endif 
        </div>
        <div class="video-view">
          {{ __('main.Size')}} : {{ $TrendPeliculas->seo_title }} {{ __('main.MB')}} &nbsp;<i class="fas fa-calendar-alt"></i> {{ date('M j, Y', strtotime($TrendPeliculas->created_at)) }}
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- /.container-fluid -->
<!-- Sticky Footer -->
<footer class="sticky-footer">
 <div class="container">
  <div class="row no-gutters">
   <div class="col-lg-6 col-sm-6">
    <p class="mt-1 mb-0 text-dark">{{ __('main.All_reserved')}} <strong class="text-dark">{{ option('SiteTitle')  }}</strong>.<br></p>
  </div>
  <div class="col-lg-6 col-sm-6 text-right">
    <div class="app">
     <a><img alt="{{ option('SiteTitle')  }}" src="{{ asset('assets/images/gplay.png') }}"></a>
     <a><img alt="{{ option('SiteTitle')  }}" src="{{ asset('assets/images/aplay.png') }}"></a>
   </div>
 </div>
</div>
</div>
</footer>
</div>
<!-- /.content-wrapper -->
</div>
<!-- ============================================================= Content end   ============================================================= -->
@endsection