@extends('layouts.main')

@section('content')
<!-- ============================================================= Content Start ============================================================= -->
<span class="display-zero">{{ $Locale = LaravelLocalization::getCurrentLocale() }}</span>
<div id="wrapper">
 <div id="content-wrapper">
  <div class="container-fluid pb-0">
  <div class="video-block section-padding">
    <div class="row">
     <div class="col-md-12">
      <div class="main-title">
       <h6>{{ __('main.Featured_Peliculass')}}</h6>
     </div>
   </div>
   @foreach($PeliculasDes as $Peliculas)
<div class="col-xl-2 col-sm-4 mb-3">
  <div class="video-card">
   <div class="video-card-image">
    <a class="play-icon" href="{{ url('Peliculas') }}/{{$Peliculas->slug}}"></a>
    <a href="{{ url('Peliculas') }}/{{$Peliculas->slug}}">
     <img src="{{ asset($Peliculas->ImageUpload->filename) }}" alt="{{ $Peliculas->meta_description }}" class="img-fluid">
   </a>
   @if(isset($Peliculas->Category->{'Title_'.$Locale}))
   <div class="time">{{ $Peliculas->Category->{'Title_'.$Locale} }} </div>
   @else
   @endif
 </div>
 <div class="video-card-body">
  <div class="video-title">
   <a href="{{ url('Peliculas') }}/{{$Peliculas->slug}}">{{ $Peliculas->{'Title_'.$Locale} }}</a>
 </div>
 <div class="video-page text-color">
   @if(isset($Peliculas->Category->{'Title_'.$Locale}))
   {{ $Peliculas->Category->{'Title_'.$Locale} }}
   @else
   @endif
 </div>
 <div class="video-view">
  {{ __('main.Size')}} : {{ $Peliculas->seo_title }} <i class="fas fa-calendar-alt"></i> {{ date('M j, Y', strtotime($Peliculas->created_at)) }}
</div>
</div>
</div>
</div>
@endforeach
</div>
</div>
<nav aria-label="Page navigation example">
 <ul class="pagination justify-content-center pagination-sm mb-4">
  <!-- ================ /New Teams content =================== -->
  {{ $PeliculasDes->links() }}
  <!-- ================ /New Teams content =================== -->
</ul>
</nav>
<hr class="mt-0">
<div class="single-channel-image section-padding mb-4">
<!-- ================ /New Adones content =================== -->
 @foreach(AdTwos() as $Ad)
@if($Ad->Type == 'image')
 <a>
  <img class="img-fluid" src="{{ asset($Ad->url) }}" alt="{{ $Ad->Display }}" width="100%">
</a>
@elseif($Ad->Type == 'Code')
{{ $Ad->code }}
@endif
@endforeach
<!-- ================ /New Adones content =================== -->
</div>
</div>
<!-- /.container-fluid -->
<!-- Sticky Footer -->
<footer class="sticky-footer">
 <div class="container">
  <div class="row no-gutters">
   <div class="col-lg-6 col-sm-6">
    <p class="mt-1 mb-0 text-dark">{{ __('main.All_reserved')}} <strong class="text-dark">{{ option('SiteTitle')  }}</strong>.<br></p>
  </div>
  <div class="col-lg-6 col-sm-6 text-right">
    <div class="app">
     <a><img alt="{{ option('SiteTitle')  }}" src="{{ asset('assets/images/gplay.png') }}"></a>
     <a><img alt="{{ option('SiteTitle')  }}" src="{{ asset('assets/images/aplay.png') }}"></a>
   </div>
 </div>
</div>
</div>
</footer>
</div>
<!-- /.content-wrapper -->
</div>
<!-- ============================================================= Content end   ============================================================= -->
@endsection