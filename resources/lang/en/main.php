<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */


    'Browse_categories' => 'Categorías de películas',
    'Trend_Peliculass' => 'Tendencias de películas y programas de televisión',
    'Corner_Download' => 'Descargar películas & Series',
    'Available_in' => 'Disponible en:',
    'Size' => 'Tamaño',
    'MB' => 'MB',
    'Featured_Peliculass' => 'Películas destacadas',
    'Popular_Series' => 'Series de TV populares',
    'Peliculass' => 'Películas',
    'Category' => 'Categoría',
    'Login' => 'Iniciar sesión',
    'Register' => 'Registrarse',
    'Logout' => 'Cerrar sesión',
    'Search' => 'qué películas ver ...',
    'About' => 'Acerca de',
    'Download' => 'Descargas',
    'Inc' => 'Inc',
    'All_reserved' => 'Todos los derechos reservados 2020 ',
    'Popular_leagues' => 'Ligas populares',
    'Address' => 'Address',
    'E-Mail_Address' => 'E-Mail Address',
    'Password' => 'Password',
    'Remember_Me' => 'Recuérdame',
    'Forgot_Password' => '¿Olvidaste tu contraseña?',
    'Sign_in' => 'Registrarse',
    'Name' => 'Nombre',
    'Confirm_Password' => 'Confirmar contraseña',
    'Sorry' => 'Lo sentimos pero no pudimos encontrar esta página',
    'This_exsist' => 'Esta página que busca no existe',
    'Home' => 'Inicio',
    'languages' => 'sitio de idiomas',
    
    
];
