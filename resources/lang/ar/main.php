<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'Browse_categories' => 'فئات الأفلام',
    'Trend_Peliculass' => 'أفلام تريند والبرامج التلفزيونية',
    'Corner_Download' => 'ركن تحميل الافلام',
    'Available_in' => 'متوفر في:',
    'Size' => 'بحجم',
    'MB' => 'ميغابايت',
    'Featured_Peliculass' => 'أفلام مميزة',
    'Popular_Series' => 'برنامج تلفزيوني شعبي',
    'Peliculass' => 'أفلام',
    'Category' => 'الفئة',
    'Login' => 'تسجيل الدخول',
    'Register' => 'تسجيل',
    'Logout' => 'تسجيل خروج',
    'Search' => 'ما الأفلام لمشاهدتها ...',
    'About' => 'حول',
    'Download' => 'تحميل',
    'Inc' => 'تتضمن',
    'All_reserved' => 'جميع الحقوق محفوظة 2020 ',
    'Popular_leagues' => 'الدوريات الشعبية',
    'Address' => 'عنوان',
    'E-Mail_Address' => 'عنوان البريد الإلكتروني',
    'Password' => 'كلمه السر',
    'Remember_Me' => 'تذكرنى',
    'Forgot_Password' => 'نسيت رقمك السري؟',
    'Sign_in' => 'تسجيل الدخول',
    'Name' => 'اسم',
    'Confirm_Password' => 'تأكيد كلمة المرور',
    'Sorry' => 'عذرًا ، لم نتمكن من العثور على هذه الصفحة',
    'This_exsist' => 'هذه الصفحة التي تبحث عنها غير موجودة',
    'Home' => 'الصفحة الرئيسية',
    'languages' => 'موقع لغات',
];

