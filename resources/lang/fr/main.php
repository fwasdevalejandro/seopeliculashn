<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'Browse_categories' => 'Catégories de films',
    'Trend_Peliculass' => 'Films et séries télé tendance',
    'Corner_Download' => 'Seo-Peliculas Télécharger des films',
    'Available_in' => 'Disponible en:',
    'Size' => 'Taille',
    'MB' => 'MB',
    'Featured_Peliculass' => 'Films en vedette',
    'Popular_Series' => 'Séries télévisées populaires',
    'Peliculass' => 'Films',
    'Category' => 'Catégorie',
    'Login' => 'Login',
    'Register' => 'Register',
    'Logout' => 'Logout',
    'Search' => 'quels films regarder ...',
    'About' => 'About',
    'Download' => 'Download',
    'Inc' => 'Inc',
    'All_reserved' => 'All rights reserved 2020 ',
    'Popular_leagues' => 'Popular leagues',
    'Address' => 'Address',
    'E-Mail_Address' => 'E-Mail Address',
    'Password' => 'Password',
    'Remember_Me' => 'Remember Me',
    'Forgot_Password' => 'Forgot Your Password?',
    'Sign_in' => 'Sign in',
    'Name' => 'Name',
    'Confirm_Password' => 'Confirm Password',
    'Sorry' => 'Sorry but we couldnt Find This Page',
    'This_exsist' => 'This Page you are looking For Does Not Exsist',
    'Home' => 'Home',
    'languages' => 'languages Site',
];
