-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.7.24 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para seo-peliculas
CREATE DATABASE IF NOT EXISTS `seo-peliculas` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `seo-peliculas`;

-- Volcando estructura para tabla seo-peliculas.ad_senses
CREATE TABLE IF NOT EXISTS `ad_senses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Display` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `Active` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `url` longtext COLLATE utf8mb4_unicode_ci,
  `code` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla seo-peliculas.ad_senses: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `ad_senses` DISABLE KEYS */;
INSERT INTO `ad_senses` (`id`, `Location`, `name`, `Display`, `Type`, `Active`, `url`, `code`, `created_at`, `updated_at`) VALUES
	(18, 'Home', 'homeones', 'homeones', 'image', 'on', 'images/channel-banner.png', '<p><br></p>', '2020-03-12 12:26:29', '2020-03-23 22:15:35'),
	(19, 'Home', 'AdTwo', 'AdTwos', 'image', 'on', 'images/channel-banner.png', '<p><br></p>', '2020-03-12 12:37:16', '2020-03-23 22:15:51'),
	(20, 'Single', 'single', 'single', 'image', 'on', 'images/channel-banner.png', '<p><br></p>', '2020-03-14 10:33:20', '2020-03-23 22:16:02');
/*!40000 ALTER TABLE `ad_senses` ENABLE KEYS */;

-- Volcando estructura para tabla seo-peliculas.categorias
CREATE TABLE IF NOT EXISTS `categorias` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order` int(11) DEFAULT '1',
  `Title_ar` longtext COLLATE utf8mb4_unicode_ci,
  `Title_en` longtext COLLATE utf8mb4_unicode_ci,
  `Title_fr` longtext COLLATE utf8mb4_unicode_ci,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla seo-peliculas.categorias: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` (`id`, `parent_id`, `order`, `Title_ar`, `Title_en`, `Title_fr`, `slug`, `color`, `created_at`, `updated_at`) VALUES
	(27, 1, 1, 'خيال', 'Ficción', 'Fiction', 'fiction', 'danger', '2020-02-15 17:39:56', '2020-06-01 01:42:41'),
	(28, 2, 2, 'أجراءات', 'Acción', 'Action', 'action', 'danger', '2020-02-15 17:40:39', '2020-06-01 01:43:00'),
	(29, 3, 3, 'إستراتيجية', 'Estrategia', 'Strategy', 'strategy', 'danger', '2020-02-15 17:41:11', '2020-06-01 01:43:31'),
	(30, 4, 4, 'رعب', 'Horror', 'Horror', 'horror', 'primary', '2020-02-15 17:41:42', '2020-02-15 17:41:42'),
	(31, 5, 5, 'قتال', 'Lucha', 'Fight', 'fight', 'primary', '2020-02-15 17:43:20', '2020-06-01 01:44:15'),
	(34, 6, 2, 'عرض تلفزيوني', 'TV Shows', 'TV Shows', 'tv-shows', 'primary', '2020-03-09 12:04:18', '2020-03-09 12:04:18');
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;

-- Volcando estructura para tabla seo-peliculas.clients
CREATE TABLE IF NOT EXISTS `clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Title_ar` longtext COLLATE utf8mb4_unicode_ci,
  `Title_en` longtext COLLATE utf8mb4_unicode_ci,
  `Title_fr` longtext COLLATE utf8mb4_unicode_ci,
  `body_ar` longtext COLLATE utf8mb4_unicode_ci,
  `body_en` longtext COLLATE utf8mb4_unicode_ci,
  `body_fr` longtext COLLATE utf8mb4_unicode_ci,
  `ImageUpload_id` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla seo-peliculas.clients: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;

-- Volcando estructura para tabla seo-peliculas.comments
CREATE TABLE IF NOT EXISTS `comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Post_id` int(11) NOT NULL DEFAULT '1',
  `User_id` int(11) NOT NULL DEFAULT '1',
  `Comment` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla seo-peliculas.comments: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Volcando estructura para tabla seo-peliculas.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla seo-peliculas.failed_jobs: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Volcando estructura para tabla seo-peliculas.galleries
CREATE TABLE IF NOT EXISTS `galleries` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Title_ar` longtext COLLATE utf8mb4_unicode_ci,
  `Title_en` longtext COLLATE utf8mb4_unicode_ci,
  `Title_fr` longtext COLLATE utf8mb4_unicode_ci,
  `body_ar` longtext COLLATE utf8mb4_unicode_ci,
  `body_en` longtext COLLATE utf8mb4_unicode_ci,
  `body_fr` longtext COLLATE utf8mb4_unicode_ci,
  `Prize` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Platform` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Player` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ImageUpload_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla seo-peliculas.galleries: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `galleries` DISABLE KEYS */;
/*!40000 ALTER TABLE `galleries` ENABLE KEYS */;

-- Volcando estructura para tabla seo-peliculas.image_uploads
CREATE TABLE IF NOT EXISTS `image_uploads` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `filename` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=399 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla seo-peliculas.image_uploads: ~25 rows (aproximadamente)
/*!40000 ALTER TABLE `image_uploads` DISABLE KEYS */;
INSERT INTO `image_uploads` (`id`, `filename`, `created_at`, `updated_at`) VALUES
	(327, 'images/favicon-32x32.png', '2020-03-09 13:16:49', '2020-03-09 13:16:49'),
	(336, 'images/logo.png', '2020-03-09 13:32:23', '2020-03-09 13:32:23'),
	(360, 'images/ads1.png', '2020-03-14 10:33:18', '2020-03-14 10:33:18'),
	(377, 'images/v1.png', '2020-03-15 11:28:44', '2020-03-15 11:28:44'),
	(378, 'images/v1.png', '2020-03-15 11:28:49', '2020-03-15 11:28:49'),
	(379, 'images/v1.png', '2020-03-15 11:28:53', '2020-03-15 11:28:53'),
	(380, 'images/v1.png', '2020-03-15 11:28:58', '2020-03-15 11:28:58'),
	(381, 'images/v1.png', '2020-03-15 11:29:05', '2020-03-15 11:29:05'),
	(382, 'images/v1.png', '2020-03-15 11:29:34', '2020-03-15 11:29:34'),
	(383, 'images/v1.png', '2020-03-15 11:29:38', '2020-03-15 11:29:38'),
	(384, 'images/v1.png', '2020-03-15 11:29:46', '2020-03-15 11:29:46'),
	(385, 'images/v1.png', '2020-03-15 11:29:49', '2020-03-15 11:29:49'),
	(386, 'images/v1.png', '2020-03-15 11:29:54', '2020-03-15 11:29:54'),
	(387, 'images/v1.png', '2020-03-15 11:30:35', '2020-03-15 11:30:35'),
	(388, 'images/v1.png', '2020-03-15 11:30:52', '2020-03-15 11:30:52'),
	(389, 'images/v1.png', '2020-03-15 11:30:56', '2020-03-15 11:30:56'),
	(390, 'images/v1.png', '2020-03-15 11:31:00', '2020-03-15 11:31:00'),
	(391, 'images/v1.png', '2020-03-15 11:31:04', '2020-03-15 11:31:04'),
	(392, 'images/v1.png', '2020-03-15 11:31:10', '2020-03-15 11:31:10'),
	(393, 'images/v1.png', '2020-03-15 11:31:20', '2020-03-15 11:31:20'),
	(394, 'images/v1.png', '2020-03-15 11:31:24', '2020-03-15 11:31:24'),
	(395, 'images/v1.png', '2020-03-15 11:31:29', '2020-03-15 11:31:29'),
	(396, 'images/channel-banner.png', '2020-03-15 11:32:43', '2020-03-15 11:32:43'),
	(397, 'images/channel-banner.png', '2020-03-15 11:32:48', '2020-03-15 11:32:48'),
	(398, 'images/Icon24.png', '2020-05-24 07:43:51', '2020-05-24 07:43:51');
/*!40000 ALTER TABLE `image_uploads` ENABLE KEYS */;

-- Volcando estructura para tabla seo-peliculas.instagrams
CREATE TABLE IF NOT EXISTS `instagrams` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Title_ar` longtext COLLATE utf8mb4_unicode_ci,
  `Title_en` longtext COLLATE utf8mb4_unicode_ci,
  `Title_fr` longtext COLLATE utf8mb4_unicode_ci,
  `body_ar` longtext COLLATE utf8mb4_unicode_ci,
  `body_en` longtext COLLATE utf8mb4_unicode_ci,
  `body_fr` longtext COLLATE utf8mb4_unicode_ci,
  `ImageUpload_id` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla seo-peliculas.instagrams: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `instagrams` DISABLE KEYS */;
/*!40000 ALTER TABLE `instagrams` ENABLE KEYS */;

-- Volcando estructura para tabla seo-peliculas.menus
CREATE TABLE IF NOT EXISTS `menus` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Title` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla seo-peliculas.menus: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` (`id`, `Title`, `created_at`, `updated_at`) VALUES
	(1, 'Main-menu', '2020-01-02 11:39:50', '2020-01-02 11:39:50');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;

-- Volcando estructura para tabla seo-peliculas.menu_items
CREATE TABLE IF NOT EXISTS `menu_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT '0',
  `order` int(11) DEFAULT '0',
  `Title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Title_fr` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla seo-peliculas.menu_items: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` (`id`, `menu_id`, `order`, `Title_en`, `Title_ar`, `Title_fr`, `url`, `target`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 'Inicio', 'الصفحة الرئيسية', 'Accueil', '/', '_blank', '2020-01-13 10:48:19', '2020-01-13 10:48:19'),
	(11, 1, 2, 'Peliculas', 'افلام جديده', 'Movies', 'Peliculas', '_blank', '2020-03-12 12:52:38', '2020-03-12 12:52:38');
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;

-- Volcando estructura para tabla seo-peliculas.messages
CREATE TABLE IF NOT EXISTS `messages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Subject` longtext COLLATE utf8mb4_unicode_ci,
  `name` longtext COLLATE utf8mb4_unicode_ci,
  `mail` longtext COLLATE utf8mb4_unicode_ci,
  `User_id` longtext COLLATE utf8mb4_unicode_ci,
  `Message` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla seo-peliculas.messages: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;

-- Volcando estructura para tabla seo-peliculas.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla seo-peliculas.migrations: ~18 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2019_12_09_143619_create_permission_tables', 2),
	(5, '2016_09_13_070520_add_verification_to_user_table', 3),
	(6, '2014_10_12_000000_create_users_table', 4),
	(7, '2019_12_29_153111_create_image_uploads_table', 5),
	(8, '2019_12_30_141843_create_posts_table', 6),
	(9, '2019_12_30_142016_create_categories_table', 7),
	(10, '2019_12_30_142116_create_clients_table', 7),
	(11, '2019_12_30_142137_create_ad_senses_table', 7),
	(12, '2019_12_30_142156_create_menus_table', 7),
	(13, '2019_12_30_142250_create_menu_items_table', 7),
	(14, '2019_12_30_142326_create_galleries_table', 7),
	(15, '2019_12_30_142339_create_instagrams_table', 7),
	(16, '2019_12_30_142418_create_messages_table', 7),
	(17, '2019_12_30_170524_create_comments_table', 8),
	(18, '2019_12_30_171111_create_menu_items_table', 9),
	(19, '2017_03_03_100000_create_options_table', 10);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando estructura para tabla seo-peliculas.model_has_permissions
CREATE TABLE IF NOT EXISTS `model_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla seo-peliculas.model_has_permissions: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
INSERT INTO `model_has_permissions` (`permission_id`, `model_type`, `model_id`) VALUES
	(31, 'App\\User', 43);
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;

-- Volcando estructura para tabla seo-peliculas.model_has_roles
CREATE TABLE IF NOT EXISTS `model_has_roles` (
  `role_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla seo-peliculas.model_has_roles: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
	(30, 'App\\User', 43),
	(34, 'App\\User', 43);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;

-- Volcando estructura para tabla seo-peliculas.options
CREATE TABLE IF NOT EXISTS `options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `options_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla seo-peliculas.options: ~34 rows (aproximadamente)
/*!40000 ALTER TABLE `options` DISABLE KEYS */;
INSERT INTO `options` (`id`, `key`, `value`) VALUES
	(2, 'Home_fr', 'Seo-Peliculas Todo con SEO.'),
	(3, 'Home_ar', 'كورنر - افلام قم بتنزيل وعرض البرنامج النصي \r\nهو تصميم مستجيب تمامًا لمواقع الأفلام والبرامج النصية الخاصة بتنزيل الأفلام'),
	(4, 'Home_en', 'Seo-Peliculas Todo con SEO.'),
	(5, 'Favicon', 'images/favicon-32x32.png'),
	(6, 'LinkTwo', 'LinkTwo'),
	(7, 'Link', 'Link'),
	(8, 'youtube', 'youtube'),
	(9, 'GitHub', 'GitHub'),
	(10, 'Twitter', 'Twitter'),
	(11, 'Pinterest', 'Pinterest'),
	(12, 'Tumblr', 'Tumblr'),
	(13, 'Snapchat', 'Snapchat'),
	(14, 'LinkedIn', 'LinkedIn'),
	(15, 'Instagram', 'Instagram'),
	(16, 'Facebook', 'Facebook'),
	(17, 'MetaKeyWords', 'Seo-Peliculas Todo con SEO.'),
	(18, 'MetaDescription', 'Seo-Peliculas Todo con SEO.'),
	(19, 'video', 'video'),
	(20, 'Googlemap', 'Googlemap'),
	(21, 'Email', 'seo_peliculas@gmail.com'),
	(22, 'PhoneNumber', '0220-10022-52'),
	(23, 'Address', 'London- jerey - 2050'),
	(24, 'SiteTitle', 'SEO-Peliculas'),
	(25, 'Language', 'Language'),
	(28, 'Metaauthor', 'author'),
	(29, 'Metarobots', 'Metarobots'),
	(30, 'coveruser', 'images/channel-banner.png'),
	(31, 'Server', 'pelis-series.com'),
	(32, 'covernew', 'images/channel-banner.png'),
	(33, 'coverMessage', 'images/channel-banner.png'),
	(34, 'coverAdSense', 'images/channel-banner.png'),
	(35, 'coverInstagrams', 'images/channel-banner.png'),
	(36, 'coverSettings', 'images/channel-banner.png'),
	(37, 'logo', 'images/logo.png');
/*!40000 ALTER TABLE `options` ENABLE KEYS */;

-- Volcando estructura para tabla seo-peliculas.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla seo-peliculas.password_resets: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
	('admin@admin.com', '$2y$10$bui5fl4azCrQhE2fjJQ6YOvAdyRJHXJVsJeAYuVLNWOGWCl9QFXM2', '2019-12-09 14:13:59');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Volcando estructura para tabla seo-peliculas.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla seo-peliculas.permissions: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
	(31, 'edit articles', 'web', '2020-01-04 14:37:28', '2020-01-04 14:37:28'),
	(35, 'User', 'web', '2020-01-04 16:05:35', '2020-01-04 16:05:35');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Volcando estructura para tabla seo-peliculas.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) DEFAULT NULL,
  `categorias_id` int(11) DEFAULT NULL,
  `Title_ar` longtext COLLATE utf8mb4_unicode_ci,
  `Title_en` longtext COLLATE utf8mb4_unicode_ci,
  `Title_fr` longtext COLLATE utf8mb4_unicode_ci,
  `body_ar` longtext COLLATE utf8mb4_unicode_ci,
  `body_en` longtext COLLATE utf8mb4_unicode_ci,
  `body_fr` longtext COLLATE utf8mb4_unicode_ci,
  `ImageUpload_id` int(11) DEFAULT NULL,
  `slug` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci,
  `meta_keywords` longtext COLLATE utf8mb4_unicode_ci,
  `seo_title` longtext COLLATE utf8mb4_unicode_ci,
  `Downloud` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT 'out',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla seo-peliculas.posts: ~19 rows (aproximadamente)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` (`id`, `author_id`, `categorias_id`, `Title_ar`, `Title_en`, `Title_fr`, `body_ar`, `body_en`, `body_fr`, `ImageUpload_id`, `slug`, `meta_description`, `meta_keywords`, `seo_title`, `Downloud`, `featured`, `created_at`, `updated_at`) VALUES
	(54, 43, 27, 'مرآة سوداء', 'Black Mirror', 'Black Mirror', 'مرآة سوداء', 'Black Mirror', 'Black Mirror', 392, 'black-mirror', 'مرآة سوداء', 'https://www.youtube.com/embed/UlT408RXZJU', '20', 'https://www90.zippyshare.com/v/EiFYEUxG/file.html', 'on', '2020-03-09 13:49:45', '2020-03-15 11:31:10'),
	(55, 43, 27, 'نهاية الرحلة', 'Journey\'s End', 'Journey\'s End', 'نهاية الرحلة', 'Journey\'s End', 'Journey\'s End', 393, 'journey-s-end', 'Journey\'s End', 'https://www.youtube.com/embed/UlT408RXZJU', '20', 'https://www90.zippyshare.com/v/EiFYEUxG/file.html', 'on', '2020-03-09 13:51:34', '2020-03-15 11:31:20'),
	(56, 43, 27, 'الحب ، سيمون', 'Love, Simon', 'Love, Simon', 'الحب ، سيمون', 'Love Simon', 'Love, Simon', 394, 'love-simon', 'Love, Simon', 'https://www.youtube.com/embed/UlT408RXZJU', '230', 'https://www90.zippyshare.com/v/EiFYEUxG/file.html', 'on', '2020-03-09 13:52:52', '2020-03-15 11:31:25'),
	(57, 43, 27, 'نفس', 'Breath', 'Breath', 'نفس', 'Breath', 'Breath', 395, 'breath', 'Breath', 'https://www.youtube.com/embed/UlT408RXZJU', '230', 'https://www90.zippyshare.com/v/EiFYEUxG/file.html', 'on', '2020-03-09 13:56:10', '2020-03-15 11:31:30'),
	(58, 43, 27, 'الغرباء فريسة في الليل', 'The Strangers Prey At Night', 'The Strangers Prey At Night', 'الغرباء فريسة في الليل', 'The Strangers Prey At Night', 'The Strangers Prey At Night', 391, 'the-strangers-prey-at-night', 'The Strangers Prey At Night', 'https://www.youtube.com/embed/UlT408RXZJU', '950', 'https://www90.zippyshare.com/v/EiFYEUxG/file.html', 'on', '2020-03-09 13:58:38', '2020-03-15 11:31:05'),
	(59, 43, 28, 'الشاهد الأخير', 'The Last Witness', 'The Last Witness', 'الشاهد الأخير', 'The Last Witness', 'The Last Witness', 390, 'the-last-witness', 'The Last Witness', 'https://www.youtube.com/embed/UlT408RXZJU', '206', 'https://www90.zippyshare.com/v/EiFYEUxG/file.html', 'on', '2020-03-09 14:39:59', '2020-03-15 11:31:01'),
	(60, 43, 28, 'منتصف الليل رعاة البقر', 'Mid Night Cowboy', 'Mid Night Cowboy', 'منتصف الليل رعاة البقر', 'Mid Night Cowboy', 'Mid Night Cowboy', 389, 'mid-night-cowboy', 'Mid Night Cowboy', 'https://www.youtube.com/embed/UlT408RXZJU', '230', 'https://www90.zippyshare.com/v/EiFYEUxG/file.html', 'on', '2020-03-09 14:45:22', '2020-03-15 11:30:57'),
	(61, 43, 28, 'رحمة', 'The Mercy', 'The Mercy', 'رحمة', 'The Mercy', 'The Mercy', 388, 'the-mercy', 'The Mercy', 'https://www.youtube.com/embed/UlT408RXZJU', '951', 'https://www90.zippyshare.com/v/EiFYEUxG/file.html', 'on', '2020-03-09 14:47:13', '2020-03-15 11:30:53'),
	(62, 43, 29, '12 قوي', '12 Strong', '12 Strong', '12 قوي', '12 Strong', '12 Strong', 387, '12-strong', '12 Strong', 'https://www.youtube.com/embed/UlT408RXZJU', '120', 'https://www90.zippyshare.com/v/EiFYEUxG/file.html', 'on', '2020-03-09 14:48:55', '2020-03-15 11:30:36'),
	(65, 43, 30, 'شمس منتصف الليل', 'Midnight Sun', 'Midnight Sun', 'شمس منتصف الليل', 'Midnight Sun', 'Midnight Sun', 386, 'midnight-sun', 'Midnight Sun', 'https://www.youtube.com/embed/UlT408RXZJU', '987', 'https://www90.zippyshare.com/v/EiFYEUxG/file.html', 'on', '2020-03-09 14:58:06', '2020-03-15 11:29:54'),
	(66, 43, 30, 'التراب', 'Dirt', 'Dirt', 'التراب', 'Dirt', 'Dirt', 385, 'dirt', 'Dirt', 'https://www.youtube.com/embed/UlT408RXZJU', '963', 'https://www90.zippyshare.com/v/EiFYEUxG/file.html', 'on', '2020-03-09 14:59:40', '2020-03-15 11:29:50'),
	(67, 43, 30, 'أتمنى لك نهارا سعيد', 'Have a Nice Day', 'Have a Nice Day', 'أتمنى لك نهارا سعيد', 'Have a Nice Day', 'Have a Nice Day', 384, 'have-a-nice-day', 'Have a Nice Day', 'https://www.youtube.com/embed/UlT408RXZJU', '984', 'https://www90.zippyshare.com/v/EiFYEUxG/file.html', 'on', '2020-03-09 15:01:23', '2020-03-15 11:29:46'),
	(68, 43, 28, 'مقتل ال', 'The killing Of The', 'The killing Of The', 'مقتل ال', 'The killing Of The', 'The killing Of The', 383, 'the-killing-of-the', 'The killing Of The', 'https://www.youtube.com/embed/UlT408RXZJU', '230', 'https://www90.zippyshare.com/v/EiFYEUxG/file.html', 'on', '2020-03-09 15:05:56', '2020-03-15 11:29:39'),
	(69, 43, 34, 'شيكاغو ميد', 'Chicago Med', 'Chicago Med', 'شيكاغو ميد', 'Chicago Med', 'Chicago Med', 382, 'chicago-med', 'Chicago Med', 'https://www.youtube.com/embed/UlT408RXZJU', '963', 'https://www90.zippyshare.com/v/EiFYEUxG/file.html', 'on', '2020-03-09 15:14:31', '2020-03-15 11:29:35'),
	(70, 43, 34, 'تشريح غراي', 'Grey\'s anatomy', 'Grey\'s anatomy', 'تشريح غراي', 'Grey anatomy', 'Grey\'s anatomy', 381, 'grey-s-anatomy', 'Grey\'s anatomy', 'https://www.youtube.com/embed/UlT408RXZJU', '789', 'https://www90.zippyshare.com/v/EiFYEUxG/file.html', 'on', '2020-03-09 15:15:23', '2020-03-15 11:29:06'),
	(71, 43, 34, 'آخر رجل على الأرض', 'The Last Man on the earth', 'The Last Man on the earth', 'آخر رجل على الأرض', 'The Last Man on the earth', 'The Last Man on the earth', 380, 'the-last-man-on-the-earth', 'The Last Man on the earth', 'https://www.youtube.com/embed/UlT408RXZJU', '963', 'https://www90.zippyshare.com/v/EiFYEUxG/file.html', 'on', '2020-03-09 15:24:09', '2020-03-15 11:28:59'),
	(72, 43, 34, 'غير قابلة للكسر كيمي شميت', 'Unbreakable Kimmy Schmidt', 'Unbreakable Kimmy Schmidt', 'غير قابلة للكسر كيمي شميت', 'Unbreakable Kimmy Schmidt', 'Unbreakable Kimmy Schmidt', 379, 'unbreakable-kimmy-schmidt', 'Unbreakable Kimmy Schmidt', 'https://www.youtube.com/embed/UlT408RXZJU', '789', 'https://www90.zippyshare.com/v/EiFYEUxG/file.html', 'on', '2020-03-09 15:27:45', '2020-03-15 11:28:54'),
	(73, 43, 34, 'بيت البطاقات', 'House of cards', 'House of cards', 'بيت البطاقات', 'House of cards', 'House of cards', 378, 'house-of-cards', 'House of cards', 'https://www.youtube.com/embed/UlT408RXZJU', '230', 'https://www90.zippyshare.com/v/EiFYEUxG/file.html', 'on', '2020-03-09 15:29:34', '2020-03-15 11:28:49'),
	(74, 43, 34, 'دوم جروي', 'Dom grozy', 'Dom grozy', 'دوم جروي', 'Dom grozy', 'Dom grozy', 377, 'dom-grozy', 'Dom grozy', 'https://www.youtube.com/embed/UlT408RXZJU', '963', 'https://www90.zippyshare.com/v/EiFYEUxG/file.html', 'on', '2020-03-09 15:30:28', '2020-03-15 11:28:45');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Volcando estructura para tabla seo-peliculas.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla seo-peliculas.roles: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
	(30, 'Super-Admin', 'web', '2020-01-04 14:37:28', '2020-01-04 14:37:28'),
	(34, 'User', 'web', '2020-01-04 16:05:35', '2020-01-04 16:05:35');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Volcando estructura para tabla seo-peliculas.role_has_permissions
CREATE TABLE IF NOT EXISTS `role_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla seo-peliculas.role_has_permissions: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;

-- Volcando estructura para tabla seo-peliculas.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ImageUpload_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '335',
  `Phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla seo-peliculas.users: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `ImageUpload_id`, `Phone`, `remember_token`, `created_at`, `updated_at`) VALUES
	(43, 'Ghost', 'lorddexstar007@gmail.com', NULL, '$2y$10$.bAJ0pHYFfw1XcpUKM.WD.VSE2TAsmhzTLaRzesvTcFTSrUGFNOqy', '398', '45465', 'qbme24swU5R4l3TgvwGtU8uHnGNO5zc91NYWtbmUAph8XBjL5mZDMSMnlG5X', '2020-01-25 15:16:19', '2020-05-24 07:44:18');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
