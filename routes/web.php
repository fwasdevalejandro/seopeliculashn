<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| LaravelLocalization
|--------------------------------------------------------------------------
|
*/

Route::group([
	'prefix' => LaravelLocalization::setLocale(),
	'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
],
function () {


	    Route::get('/', 'HomeController@index')->name('index');
        Route::get('search', 'SearchController@search')->name('search');
        Route::group(['middleware' => ['isVerified']], function () {
        	Route::get('email-verification/error', 'Auth\RegisterController@getVerificationError')->name('email-verification.error');
        	Route::get('email-verification/check/{token}', 'Auth\RegisterController@getVerification')->name('email-verification.check');
        });
        /*
        |--------------------------------------------------------------------------
        | Auth Routes
        |--------------------------------------------------------------------------
        |
        */
	    Auth::routes();
        /*
        |--------------------------------------------------------------------------
        | missing RETURN 404 PAGE Route
        |--------------------------------------------------------------------------
        |
        */
        Route::get('missing', function () {
        	return view('404');
        });
        /*
        |--------------------------------------------------------------------------
        | resource
        |--------------------------------------------------------------------------
        |
        */
		Route::resource('Cat','Categoriass');
		Route::get('/Peliculas/{slug}','Categoriass@ShowPelicula');
        Route::resource('Peliculass','Peliculas');
	     /**
	     * Show the middleware dashboard Super-Admin.
	     *
	     * @return \Illuminate\Contracts\Support\Renderable
	     */
	     Route::group(['middleware' => ['auth','role:Super-Admin']], function () {

		    /*
			|--------------------------------------------------------------------------
			| Web Routes Dashboard
			|--------------------------------------------------------------------------
			|
			| Here is where you can register web routes for your application. These
			| routes are loaded by the RouteServiceProvider within a group which
			| contains the "web" middleware group. Now create something great!
			|
			*/

		


			Route::resource('admin','Dashboard\Dashboard');
			Route::resource('Dashboard/Roles','Dashboard\Roles');
			Route::resource('Dashboard/Users','Dashboard\Users');
			Route::resource('Dashboard/AdSense','Dashboard\AdSenses');
			
			Route::resource('Dashboard/Clients','Dashboard\Clients');
			Route::resource('Dashboard/Galleries','Dashboard\Galleries');
			Route::resource('Dashboard/Instagrams','Dashboard\Instagrams');
			Route::resource('Dashboard/Links','Dashboard\Links');
			Route::resource('Dashboard/Menus','Dashboard\Menus');
			Route::resource('Dashboard/Media','Dashboard\Media');
			Route::resource('Dashboard/Messages','Dashboard\Messages');
			Route::resource('Dashboard/Posts','Dashboard\Posts');
			Route::resource('Dashboard/Seo','Dashboard\Seo');
			Route::resource('Dashboard/Settings','Dashboard\Settings');

		    });


});
Route::group(['middleware' => ['auth','role:Super-Admin']], function () {
Route::get('Dashboard/image/upload','ImageUploadController@fileCreate');
Route::post('Dashboard/image/upload/store','ImageUploadController@fileStore');
Route::post('Dashboard/image/delete','ImageUploadController@fileDestroy');
});
